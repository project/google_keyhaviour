Google Keyhaviour Module

danielb@aspedia.net 
http://www.aspedia.net

Aids Google AdSense campaigns by generating content (Titles, Slogans, Blocks) based on the keywords the user has put into Google

Instructions:

Open up the template.php file in your theme and find a function called _phptemplate_variables().  Near the end of that function you will find a line that says:

    return $vars;

You must change this to:

    return google_keyhaviour($hook, $vars); 

Upload the "google_keyhaviour" directory into your "modules" directory.   Activate the Google_Keyhaviour module on your admin backend as usual.  Under Site Configuration you will have a new settings screen to configure rules and their associated actions.